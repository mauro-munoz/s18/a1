let trainer = {};

trainer.name = `mauro`;
trainer.age = 30;
trainer.pokemon = [`pikachu`,`charmander`];
trainer.friends = {
	hoenn: [`Mark`,`Miguel`],
	kanto: [`Mang`,`Thomas`]
};
trainer.talk = function(pokemon){
	if (trainer.pokemon.includes(pokemon)){
		console.log(`${pokemon} I choose you!`)
	} else {`${pokemon} is not in your pokedex`}
}
console.log(trainer);
console.log(`Hi I'm ${trainer.name}, I am a pokemon master, I have ${trainer.pokemon.length} pokemon ${trainer.pokemon.join()}`)
console.log(trainer.talk(`pikachu`));

//Object Constructor

function pokeBuild (name,lvl,health,attack) {
	this.name = name;
	this.lvl = lvl;
	this.health = health * lvl;
	this.attack = attack * lvl;

	this.tackle = function(opponent){
		let x = 0
		console.log(`${this.name} used tackle on ${opponent.name}`)
		for(x ;opponent.health>0 ; x++ ){
			
			let randomattack = this.attack * Math.random();
			opponent.health -= randomattack;
			console.log(`${randomattack} damaged dealt, ${opponent.name} ${opponent.health} health`);
		}
		
		if (opponent.health <= 0) {
			console.log(`${this.name} attacked ${x} times`)
			opponent.faint();
		} 
	}

	this.faint = function(){
		console.log(`${this.name} has fainted Health is ${this.health}`);
		
	}
}

let PokeDex = [];

let pikachu = new pokeBuild(`pikachu`,2,50,25);
	PokeDex.push(pikachu.name);
let squirtle = new pokeBuild(`squirtle`,1,30,14);
	PokeDex.push(squirtle.name);
let charmander = new pokeBuild(`charmander`,3,60,7);
	PokeDex.push(charmander.name);

console.log(PokeDex)
console.log(squirtle);	
console.log(pikachu);
console.log(charmander);

pikachu.tackle(charmander);
// charmander.tackle(pikachu);
// pikachu.tackle(charmander);
// charmander.tackle(pikachu);

